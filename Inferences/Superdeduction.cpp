/*
 * This file is part of the source code of the software program
 * Vampire. It is protected by applicable
 * copyright laws.
 *
 * This source code is distributed under the licence found here
 * https://vprover.github.io/license.html
 * and in the source directory
 */
/**
 * @file Superdeduction.cpp
 * Implements class Superdeduction.
 */

#include "Debug/RuntimeStatistics.hpp"

#include "Lib/Environment.hpp"
#include "Lib/Set.hpp"

#include "Kernel/Clause.hpp"
#include "Kernel/Inference.hpp"
#include "Kernel/Term.hpp"
#include "Kernel/Unit.hpp"
#include "Kernel/RobSubstitution.hpp"

#include "Superdeduction.hpp"

namespace Inferences {

ClauseIterator Superdeduction::generateClauses(Clause* premise)
{
    CALL("Superdeduction::generateClauses");

    bool found = false;
    unsigned pos = 0;

    for (; pos < premise->length(); pos++) {
        if ((*premise)[pos]->functor() == (*_cl)[0]->functor() &&
            (*premise)[pos]->polarity() != (*_cl)[0]->polarity()) {
            found = true;
            break;
        }
    }

    if (!found) {
        return ClauseIterator::getEmpty();
    }

    static RobSubstitution subst;
    subst.reset();

    // cout << "    \033[1;32msuperdeduction: \033[0m" << endl;
    // cout << "    " << premise->toString() << endl;
    // cout << "    " << _cl->toString() << endl;

    if (subst.unifyArgs((*_cl)[0], 0, (*premise)[pos], 1)) {

      Clause *conclusion = new (premise->length() + _cl->length() - 2) Clause(premise->length() + _cl->length() - 2, GeneratingInference1(InferenceRule::SUPERDEDUCTION, premise));

      unsigned i = 0;
      unsigned j = 0;

      for (; i < _cl->length() - 1; i++) {
        (*conclusion)[i] = subst.apply((*_cl)[i + 1], 0);
      }
      for (; i < premise->length() + _cl->length() - 2; i++) {
        if (j == pos) { j++; }
        (*conclusion)[i] = subst.apply((*premise)[j], 1);
        j++;
      }

      return pvi(getSingletonIterator(conclusion));
    }

    // cout << "    no substitution found" << endl;

    return ClauseIterator::getEmpty();
}

} // namespace Inferences
