/*
 * This file is part of the source code of the software program
 * Vampire. It is protected by applicable
 * copyright laws.
 *
 * This source code is distributed under the licence found here
 * https://vprover.github.io/license.html
 * and in the source directory
 */
/**
 * @file Superdeduction.hpp
 * Defines class Superdeduction.
 */


#ifndef __Superdeduction__
#define __Superdeduction__

#include "Forwards.hpp"

#include "InferenceEngine.hpp"
#include "Superposition.hpp"

namespace Inferences {

class Superdeduction
: public GeneratingInferenceEngine
{
public:
  CLASS_NAME(Superdeduction);
  USE_ALLOCATOR(Superdeduction);

  Superdeduction(Clause* cl) : _cl(Clause::fromClause(cl)) {}

  ClauseIterator generateClauses(Clause* premise);

private:

    Clause* _cl;

};

}

#endif /* __Superdeduction__ */
